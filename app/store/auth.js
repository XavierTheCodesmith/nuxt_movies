export const getterTypes = {
    GET_LOGGED_IN: 'getLoggedIn',
    GET_CURRENT_USER: 'getCurrentUser',
};

export const mutationTypes = {
    LOGIN_MUTATION: 'login_mutation',
    LOGOUT_MUTATION: 'logout_mutation',
};

export const actionTypes = {
    LOGIN_ACTION: 'login_action',
    LOGOUT_ACTION: 'logout_action',
};

export const state = () => ({
    loggedIn: false,
    currentUser: null,
});

export const getters = {
    [getterTypes.GET_LOGGED_IN](state) {
        return state.loggedIn;
    },
    [getterTypes.GET_CURRENT_USER](state) {
        return state.currentUser;
    },
};

export const mutations = {
    [mutationTypes.LOGIN_MUTATION](state, user) {
        state.loggedIn = true;
        state.currentUser = user;
    },
    [mutationTypes.LOGOUT_MUTATION](state) {
        state.loggedIn = false;
        state.currentUser = null;
    },
};

export const actions = {
    [actionTypes.LOGIN_ACTION]({commit}, user) {
        commit(mutationTypes.LOGIN_MUTATION, user);
    },
    [actionTypes.LOGOUT_ACTION]({commit}) {
        localStorage.removeItem('TOKEN');
        commit(mutationTypes.LOGOUT_MUTATION);
    },
};
