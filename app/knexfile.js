module.exports = {
    development: {
        client: 'pg',
        connection: {
            host: '127.0.0.1',
            user: '',
            password: '',
            database: 'nuxt_movies',
            charset: 'utf8',
        },
        migrations: {
            directory: __dirname + '/server/knex/migrations',
        },
        seeds: {
            directory: __dirname + '/server/knex/seeds',
        },
    },
};
