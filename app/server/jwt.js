const jwt = require('jsonwebtoken');

function verifyJWT(req, res, next) {
    const token = req.headers.authorization || null;

    if (token) {
        const verify = jwt.verify(token, 'secret');

        if (verify) {
            req.user = verify.id;
        }
    }
    next();
}

function createJWTToken(user) {
    const token = jwt.sign(user, 'secret', {expiresIn: '30d'});
    return token;
}

module.exports = {
    verifyJWT,
    createJWTToken,
};
