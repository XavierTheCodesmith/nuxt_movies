const express = require('express');
const {verifyJWT} = require('../jwt');
const AuthController = require('../controllers/authController');
const auth = new AuthController();
const router = express.Router();

router.get('/current', verifyJWT, auth.currentUser);
router.post('/login', auth.loginUser);
router.post('/register', auth.createUser);

module.exports = router;
