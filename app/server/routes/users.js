const express = require('express');
const UserController = require('../controllers/userController');
const router = express.Router();
const users = new UserController();

router.get('/all', users.allUsers);
router.get('/single/:id', users.singleUser);

module.exports = router;