const express = require('express');
const {verifyJWT} = require('../jwt');
const MovieController = require('../controllers/moviesController');
const router = express.Router();
const movies = new MovieController();

router.get('/search/:title/:year', verifyJWT, movies.search);

module.exports = router;
