const ENV = 'development';
const config = require('../../knexfile')[ENV];
const knex = require('knex')(config);

module.exports = knex;
