
exports.up = function(knex) {
  return knex.schema.createTable('favorites', (table) => {
      table.increments();
      table.integer('user_id').unsigned();
      table.foreign('user_id').references('id').inTable('users');
      table.json('movies');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('favorites');
};
