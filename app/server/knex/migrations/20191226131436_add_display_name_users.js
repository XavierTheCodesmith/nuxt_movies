exports.up = function(knex) {
    return knex.schema.table('users', table => {
        table.string('display_name').notNullable().unique();
    });
};

exports.down = function(knex) {
    knex.schema.table('users', table => {
        table.dropColumn('display_name');
    });
};
