exports.seed = function(knex) {
    return knex('users').del().then(function() {
        return knex('users').insert([
            {
                id: 1,
                email: 'xavier@gmail.com',
                password: '$2a$10$uwhtikMrSFjH3AcUSu8aXe6s3UGGaMi2QZnRqupVB.F/6xbQjmYii',
                display_name: 'XavierTheCodesmith',
            },
        ]);
    });
};
