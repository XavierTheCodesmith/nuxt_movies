const db = require('../knex/index');
const {createJWTToken} = require('../jwt');
const {compareSync, hashSync, genSaltSync} = require('bcryptjs');

class AuthController {

    currentUser(req, res) {
        const user = req.user;
        return res.status(200).json({
            user: user ? user : null,
            loggedIn: !!user
        });
    }

    async loginUser(req, res) {
        const {email, password} = req.body;
        try {
            const userExist = await db.select('id', 'password').from('users').first().where({email});

            if (!userExist) {
                return res.status(400).send('User not valid');
            }

            const validPassword = compareSync(password, userExist.password);

            if (!validPassword) {
                return res.status(400).send('Bad password');
            }


            const token = createJWTToken(userExist);

            return res.status(200).json({
                token,
                user: userExist.id
            });
        } catch (err) {
            console.error(err);
            res.status(500).send('Failed to login');
        }
    }

    async createUser(req, res) {
        const {email, password, display_name} = req.body;
        const rounds = 10;
        const salt = genSaltSync(rounds);
        try {
            const userExist = await db.select('id').from('users').where({email}).first();

            if (userExist) {
                return res.status(400).send('User already exists');
            }

            const hash = hashSync(password, salt);

            const newUser = await db('users').returning(['id', 'display_name', 'email']).insert({
                email,
                display_name,
                password: hash
            });

            const token = createJWTToken(newUser[0]);

            res.status(201).json({
                user: newUser[0],
                token
            })
        } catch (err) {
            console.error(err);
            res.status(500).send('Failed to create user');
        }
    }
}


module.exports = AuthController;
