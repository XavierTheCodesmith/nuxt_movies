const db = require('../knex/index');

class UserController {

    async allUsers(req, res) {
        try {
            const users = await db.select('id', 'email', 'display_name').from('users');
            res.status(200).json(users);
        } catch (err) {
            res.status(500).send('Failed to get all users');
        }
    }

    async singleUser(req, res) {
        try {
            const {id} = req.params;
            const user = await db.select('id', 'email', 'display_name').from('users').where({id});

            if (user) {
                return res.status(200).json(user);
            }
            return res.status(404).send('User not found');
        } catch (err) {
            res.status(500).send('Error getting user');
        }
    }
}

module.exports = UserController;
