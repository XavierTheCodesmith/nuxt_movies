const db = require('../knex/index');
const config = require('../../config');
const fetch = require('node-fetch');

class MovieController {
    async search(req, res) {
        const {title, year} = req.params;
        const url = `http://www.omdbapi.com/?apikey=${config.apiKey}&t=${title}&y=${year}`;

        const request = await fetch(`${url}`);
        const json = await request.json();

        res.status(200).json(json);
    }
}

module.exports = MovieController;
